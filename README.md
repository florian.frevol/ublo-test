# Ublo - Test

Pour la réalisation de ce test technique j'ai utilisé Nodejs avec MongoDB pour la base de donnée (En ligne sur MongoDB Atlas).


## Installation

```bash
$ git clone git@gitlab.com:florian.frevol/ublo-test.git
$ cd ublo-test
$ npm install
```

## Lancement

```bash
$ node index.js
```

## Requêtes disponibles 


**Chicken - GET (All)**
```bash
localhost:8081/chicken
```

**Chicken - GET (One)**
```bash
localhost:8081/chicken/:id
```

**Chicken - POST**
```bash
localhost:8081/chicken
```

**Chicken - PUT**
```bash
localhost:8081/chicken/:id
```

**Chicken - PATCH**
```bash
localhost:8081/chicken/:id
```

**Chicken - DELETE**
```bash
localhost:8081/chicken/:id
```

**Chicken - RUN**
```bash
localhost:8081/chicken/run/:id
```

