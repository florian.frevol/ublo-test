var express = require('express');
var app = express();
var bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient
var urlAtlas = 'mongodb+srv://FlorianUser:TqITQTnaHHHSbf@cluster0.yfich.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

var server = app.listen(8081, function () {
    MongoClient.connect(urlAtlas, { useUnifiedTopology: true })
        .then(client => {
            console.log('Connected to Database')
            const db = client.db('chicken_db')
            const chickenCollection = db.collection('chicken')

            require('./routes/chicken_routes')(app, chickenCollection)
        })
        .catch(error => console.error(error))
    var host = server.address().address
    var port = server.address().port
    console.log("App listening at http://%s:%s", host, port)

})