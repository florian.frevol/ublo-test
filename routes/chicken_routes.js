var ObjectId = require('mongodb').ObjectID;

const doesIdExist = (id, chickenCollection) => {
    return new Promise((resolve, reject) => {
        if (id.length != 24) { reject(false) };
        chickenCollection.findOne({ _id: new ObjectId(id) })
            .then((result) => {
                if (result !== null && typeof result === 'object') {
                    resolve(true);
                }
                else {
                    reject(false)
                }
            })
            .catch(() => { reject(false) })
    })
}

const postDataChicken = (chickenCollection, req, res) => {
    console.log(req.body)
    if (req.body.name && req.body.weight) {
        var tmpJson = req.body;

        if (!tmpJson.isRunning)
            tmpJson.isRunning = false;
        if (!tmpJson.steps)
            tmpJson.steps = 0;
        chickenCollection.insertOne(req.body)
            .then(result => {
                console.log(result)
                res.send({ msg: "Chicken Post done correctly", status: 200 });
            })
            .catch(error => {
                console.log(error)
                res.send({ msg: "Chicken Post error", status: 400 });
            })
    } else { res.json({ msg: "Missing filds", status: 400 }) }
}

module.exports = (app, chickenCollection) => {
    app.post('/chicken', (req, res) => {
        postDataChicken(chickenCollection, req, res);
    })
    app.get('/chicken', (req, res) => {
        console.log(req)
        chickenCollection.find().toArray()
            .then(result => {
                console.log(result);
                res.send({ msg: "Chicken data get correctly", body: result, status: 200 });
            })
    })
    app.get('/chicken/:id', (req, res) => {
        doesIdExist(req.params.id, chickenCollection)
            .then(() => {
                chickenCollection.findOne({ _id: new ObjectId(req.params.id) })
                    .then((result) => {
                        console.log(result);
                        // (result === null) ? res.send({ msg: 'Wrong id', status: 400 }) :
                        res.send({ msg: 'Chicken data correctly get from id', body: result, status: 200 });
                    })
                    .catch((err) => {
                        console.log(err)
                        res.send({ msg: "Can't get chicken data from id", status: 400 });
                    })
            })
            .catch(() => {
                res.send({ msg: 'Wrong id', status: 400 });
            })
    })
    app.put('/chicken/:id', (req, res) => {
        delete req.body._id;
        doesIdExist(req.params.id, chickenCollection)
            .then(() => {
                chickenCollection.updateOne(
                    { _id: new ObjectId(req.params.id) },
                    { $set: req.body }
                )
                    .then((result) => {
                        console.log(result);
                        res.send({ msg: 'Chicken data correctly put from id', status: 200 });
                    })
                    .catch((err) => {
                        console.log(err);
                        res.send({ status: 400 })
                    })
            })
            .catch(() => {
                postDataChicken(chickenCollection, req, res);
            })
    })
    app.patch('/chicken/:id', (req, res) => {
        delete req.body._id;
        doesIdExist(req.params.id, chickenCollection)
            .then(() => {
                chickenCollection.updateOne(
                    { _id: new ObjectId(req.params.id) },
                    { $set: req.body }
                )
                    .then((result) => {
                        console.log(result);
                        res.send({ msg: 'Chicken data correctly patch from id', status: 200 });
                    })
                    .catch((err) => {
                        console.log(err);
                        res.send({ status: 400 })
                    })
            })
            .catch(() => {
                postDataChicken(chickenCollection, req, res);
            })
    })
    app.delete('/chicken/:id', (req, res) => {
        doesIdExist(req.params.id, chickenCollection)
            .then(() => {
                chickenCollection.deleteOne({ _id: new ObjectId(req.params.id) })
                    .then((result) => {
                        console.log(result);
                        res.send({ msg: 'Chicken destroy ahahahahhahaha', status: 200 });
                    })
                    .catch((err) => {
                        console.log(err);
                        res.send({ msg: 'Chicken not destroy - Error', status: 400 });
                    })
            })
            .catch(() => {
                res.send({ msg: 'Wrong id', status: 400 });
            })
    })
    app.post('/chicken/run/:id', (req, res) => {
        doesIdExist(req.params.id, chickenCollection)
            .then(() => {
                chickenCollection.updateOne(
                    { _id: new ObjectId(req.params.id) },
                    { $inc: {steps: 1} }
                )
                    .then((result) => {console.log(result);
                        res.send({msg: 'steps incremented', status: 200});
                    })
                    .catch((err) => {console.log(err);
                        res.send({msg: 'steps incremented error', status: 400});
                    })
            })
            .catch(() => {
                res.send({ msg: 'Wrong id', status: 400 });
            })
    })
}